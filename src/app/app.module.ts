import { createCustomElement } from '@angular/elements';
import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { GivtDonateComponent } from './givt-buttons/givt-donate-button';
@NgModule({
  declarations: [GivtDonateComponent],
  imports: [BrowserModule],
  entryComponents: [GivtDonateComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const donateButton = createCustomElement(GivtDonateComponent, { injector });
    customElements.define('givt-donate-button', donateButton);
  }

  ngDoBootstrap() {}
}
