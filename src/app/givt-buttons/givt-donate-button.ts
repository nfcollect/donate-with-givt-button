import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'givt-donate-button',
  template: `
    <div [style.max-width]="imageMaxWidth + 'px'">
      <img *ngIf="!hideQr" [src]="generateQrUrl()" />
      <a [href]="generateUrl()" target="_blank">
        <img src="https://cdn.givtapp.net/cdn/geef-met-givt-nl.png" />
      </a>
      <a title="Geef online via Givt!" *ngIf="!hideRegisterText" [href]="generateUrl()" target="_blank">Nog geen Givt? Geef Online!</a>
    </div>
  `,
  styles: [
    `
      img {
        width: 100%;
        height: 100%;
      }
    `
  ],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class GivtDonateComponent {
  @Input('namespace')
  public nameSpace = '';

  @Input('imagemaxwidth')
  public imageMaxWidth = 150;

  @Input('hideqr')
  public hideQr = false;

  @Input('hideregistertext')
  public hideRegisterText = false;

  public generateUrl(): string {
    const completeEddyId = `${this.nameSpace}.b${this.generateInstance()}`;
    const encodedEddyId = this.base64Encode(completeEddyId);
    return `https://givt-debug-api.azurewebsites.net/givt?code=${encodedEddyId}`;
  }

  public generateQrUrl(): string {
    const completeEddyId = `${this.nameSpace}.c${this.generateInstance()}`;
    return `https://cdn.givtapp.net/cdn/qr/${completeEddyId}.png`;
  }

  private generateInstance(): string {
    return '60000000001';
  }

  private base64Encode(toEncodeString: string): string {
    return btoa(toEncodeString);
  }
}
