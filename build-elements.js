const fs = require("fs-extra");
const concat = require("concat");
(async function build() {
  const files = [
    "./dist/donate-with-givt/runtime.js",
    "./dist/donate-with-givt/polyfills.js",
    "./dist/donate-with-givt/scripts.js",
    "./dist/donate-with-givt/main.js"
  ];
  await fs.ensureDir("donate-with-givt-release");
  await concat(files, "donate-with-givt-release/donate-with-givt-release.js");
})();